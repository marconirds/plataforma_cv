<?php

namespace Plataforma_CV\Classes;

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

$baseDir = dirname(dirname(dirname(__FILE__)));

require_once($baseDir.'/vendor/autoload.php');


class Login extends ConnectDB{

	private $table = 'users';


	public function  __construct() {

		parent::__construct();

    }

    public function getLoginAluno($matricula){

        $this->db->where('user_login',$matricula);
        $this->db->where('user_tipo',1);
        $this->db->where('user_status',1);
        return $this->db->ObjectBuilder()->getOne($this->table);
    }

    public function updateSenhaAluno($matricula,$data){

        $this->db->where('user_login', $matricula);
        $id = $this->db->update($this->table, $data);
        return $id;
    }

    public function getLoginNaoAluno($cpf){

        $this->db->where('user_login',$cpf);
        $this->db->where('user_tipo',2);
        $this->db->where('user_status',1);
        return $this->db->ObjectBuilder()->getOne($this->table);
    }

    public function updateSenhaNaoAluno($cpf,$data){

        $this->db->where('user_login', $cpf);
        $id = $this->db->update($this->table, $data);
        return $id;
    }

    public function createNewNaoAluno($data){

        $id = $this->db->insert($this->table, $data);
        return $id;
    }

    public function getLoginAdm($login){

        $this->db->where('user_login',$login);
        $this->db->where('user_tipo',0);
        $this->db->where('user_status',1);
        return $this->db->ObjectBuilder()->getOne($this->table);
    }

}