<?php

namespace Plataforma_CV\Classes;

$baseDir = dirname(dirname(dirname(__FILE__)));

require_once($baseDir.'/vendor/autoload.php');


class AbstractClass {

	public function  __construct() {

	}

	public function inverteData($data) {

		$ex = explode('-',$data);
		
		return $ex[2]."/".$ex[1]."/".$ex[0];
		
	}

	public function inverteDataToDatabase($data) {

		$ex = explode('/',$data);
		
		return $ex[2]."-".$ex[1]."-".$ex[0];
		
	}

	public function getFloatNumber($money){
		
		$cleanString = preg_replace('/([^0-9.,])/i', '', $money);
    	$onlyNumbersString = preg_replace('/([^0-9])/i', '', $money);

    	$separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;

    	$stringWithCommaOrDot = preg_replace('/([,.])/', '', $cleanString, $separatorsCountToBeErased);
    	$removedThousandSeparator = preg_replace('/(.|,)(?=[0-9]{3,}$)/', '',  $stringWithCommaOrDot);

    	return (float) str_replace(',', '.', $removedThousandSeparator);
	}

	public function removerAcentos($string){
		return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/","/(ç)/","/(Ç)/"),explode(" ","a A e E i I o O u U n N c C"),$string);
	}

}    




