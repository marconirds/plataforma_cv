<?php

namespace Plataforma_CV\Classes;

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

$baseDir = dirname(dirname(dirname(__FILE__)));

require_once($baseDir.'/vendor/autoload.php');


class Gerenciamento extends ConnectDB{

	private $table = 'users';


	public function  __construct() {

		parent::__construct();

    }

    public function getAllUser(){

        $users = $this->db->ObjectBuilder()->rawQuery("SELECT * FROM ".$this->table." WHERE user_tipo = ? OR user_tipo = ?", Array(1,2));
        return $users;
    }

    public function createNewUser($data){

        $id = $this->db->insert($this->table, $data);
        return $id;
    }

    public function updateUser($user_id,$data){

        $this->db->where('user_id', $user_id);
        $id = $this->db->update($this->table, $data);
        return $id;
    }

    public function deleteUser($user_id){

        $this->db->where('user_id',$user_id);
        $id = $this->db->delete($this->table);
        return $id;
    }

    public function updateStatusUser($user_id,$data){

        $this->db->where('user_id', $user_id);
        $id = $this->db->update($this->table, $data);
        return $id;
    }

    public function getUserInfo($user_id){

        $this->db->where('user_id',$user_id);
        return $this->db->ObjectBuilder()->getOne($this->table);
    }


}