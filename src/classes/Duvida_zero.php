<?php

namespace Plataforma_CV\Classes;

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

$baseDir = dirname(dirname(dirname(__FILE__)));

require_once($baseDir.'/vendor/autoload.php');


class Duvida_zero extends ConnectDB{

	private $table_dz       = 'duvida_zero';
	private $table_dz_aluno = 'duvida_zero_aluno';
	private $table_user     = 'users';

	public function  __construct() {

		parent::__construct();

    }

    public function getDZemAberto(){

        $this->db->where('status',true);
        return $this->db->ObjectBuilder()->get($this->table_dz);
    }

    public function getDZAnteriores($id_user){

        $this->db->where('id_user',$id_user);
        return $this->db->ObjectBuilder()->get($this->table_dz_aluno);
    }

    public function verifyAlunoHasDZ($id_user,$id_dz){

        $this->db->where('id_user',$id_user);
        $this->db->where('id_dz',$id_dz);
        return $this->db->ObjectBuilder()->getOne($this->table_dz_aluno);
    }

    public function getAllDZ(){

        return $this->db->ObjectBuilder()->get($this->table_dz);
    }

    public function getDZInfo($id_dz){

        $this->db->where('id_dz',$id_dz);
        return $this->db->ObjectBuilder()->getOne($this->table_dz);
    }

	public function createNewDZ($data){

        $id = $this->db->insert($this->table_dz, $data);
        return $id;
    }

    public function updateDZ($id_dz,$data){

        $this->db->where('id_dz', $id_dz);
        $id = $this->db->update($this->table_dz, $data);
        return $id;
    }

    public function deleteDZ($id_dz){

        $this->db->where('id_dz',$id_dz);
        $id = $this->db->delete($this->table_dz);
        return $id;
    }

    public function deleteDZAluno($id_dz_aluno){

        $this->db->where('id_dz_aluno',$id_dz_aluno);
        $id = $this->db->delete($this->table_dz_aluno);
        return $id;
    }

    public function updateStatusDZ($id_dz,$data){

        $this->db->where('id_dz', $id_dz);
        $id = $this->db->update($this->table_dz, $data);
        return $id;
    }

    public function getDZAluno($id_user,$id_dz){

        $this->db->where('id_user',$id_user);
        $this->db->where('id_dz',$id_dz);
        return $this->db->ObjectBuilder()->getOne($this->table_dz_aluno);
    }

    public function getRespotas($id_dz_aluno){

        $this->db->where('id_dz_aluno',$id_dz_aluno);
        return $this->db->ObjectBuilder()->getOne($this->table_dz_aluno);
    }


    public function createNewDZAluno($data){

        $id = $this->db->insert($this->table_dz_aluno, $data);
        return $id;
    }

    public function updateDZAluno($id_user,$id_dz,$data){

        $this->db->where('id_user', $id_user);
        $this->db->where('id_dz', $id_dz);
        $id = $this->db->update($this->table_dz_aluno, $data);
        return $id;
    }

    public function getRanking($id_dz,$type = 'adm'){

        if($type == 'adm'){
            $sql = "SELECT dz.*, user.user_nome, user.user_login, user.user_turma FROM ".$this->table_dz_aluno." dz LEFT JOIN ".$this->table_user." user ON (dz.id_user = user.user_id) ";
            $sql .="WHERE dz.id_dz = ? ORDER BY dz.nota DESC, dz.tempo ASC";
        }
        elseif($type == 'aluno'){
            $sql = "SELECT dza.*, user.user_nome, user.user_login, user.user_turma FROM ".$this->table_dz_aluno." dza LEFT JOIN ".$this->table_user." user ON (dza.id_user = user.user_id) ";
            $sql .="LEFT JOIN ".$this->table_dz." dz ON (dza.id_dz = dz.id_dz) WHERE dz.id_dz = ? AND dz.mostrar_ranking = 1 AND dza.nota IS NOT NULL ORDER BY dza.nota DESC, dza.tempo ASC";
        }

        $ranking = $this->db->ObjectBuilder()->rawQuery($sql, Array($id_dz));
        return $ranking;
    }

}