<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require_once('../../../vendor/cbschuld/browser.php/src/Browser.php');

$blockBrowser = false;
$browser = new Browser();
if(
    $browser->getBrowser() == Browser::BROWSER_OPERA OR 
    $browser->getBrowser() == Browser::BROWSER_EDGE OR 
    $browser->getBrowser() == Browser::BROWSER_IE OR  
    $browser->getBrowser() == Browser::BROWSER_SAFARI OR 
    $browser->getBrowser() == Browser::BROWSER_IPHONE OR 
    $browser->getBrowser() == Browser::BROWSER_IPOD OR 
    $browser->getBrowser() == Browser::BROWSER_ANDROID OR 
    $browser->getPlatform() == Browser::PLATFORM_IPHONE OR  
    $browser->getPlatform() == Browser::PLATFORM_IPAD OR
    $browser->getPlatform() == Browser::PLATFORM_IPAD OR
    $browser->getPlatform() == Browser::PLATFORM_JAVA_ANDROID OR
    $browser->getPlatform() == Browser::PLATFORM_SMART_TV
){
    $blockBrowser = true;
}

require_once('../../../vendor/autoload.php');

use Plataforma_CV\Classes\Duvida_zero;

$objDZ  = new Duvida_zero();
$getDZ  = $objDZ->getDZInfo($_GET['id_dz']);
?>

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><?=$getDZ->titulo?></h1>
</div>

<!-- Content Row -->
<div class="row">
    
    <!-- Content Column -->
    <div class="col-lg-12 mb-4">
        <!-- Project Card Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Instruções</h6>
            </div>
            <div class="card-body">
                <?php if($blockBrowser){ ?> 
                        <div class="text-center">
                        <img src="assets/img/icon_error.png" style="width:100px;"><br>
                        <span>Navegador ou plataforma não suportado! Por favor, acesse o sistema utilizando apenas o <b>Google Chrome</b> ou <b>Mozilla Firefox</b> no PC.</span>
                        </div> 
                    <?php } else{ ?>
                <ul>
                    <li>Depois de iniciado o dúvida zero, não será mais possível desistir do exame.</li>
                    <li>Durante a prova, não recarregue e não feche a página em hipótese alguma, isso resultará na perca do exame. Só inicie a prova se realmente tiver disponível no mínimo <b>2:30 hrs</b> do seu tempo.</li>
                    <li>Você terá <b>150 minutos (2:30 hrs)</b> para responder todas as questões. Ao finalizar o tempo sem a entrega da prova, a página será atualizada e apenas serão contabilizadas as questões na qual estiverem respondidas.</li>
                    <li>Há um cronômetro na página para você acompanhar o tempo restante de prova. O cronômetro é baseado em minutos e segundos.</li>
                    <li>Ao final da página, após a última questão, você encontrará um botão para realizar a entrega da prova. Também há um botão contendo uma seta para retornar ao topo da página, caso precise.</li>
                    <li>Para facilitar a visualização das questões, você pode recuar a barra lateral ao lado clicando no botão indicado.</li>
                    <li>O exame apenas será liberado se aberto em um computador, no navegadores Google Chrome e Mozilla Firefox em versões atualizadas.</li>
                </ul>

                <br>
                <div class="row">
                    <div class="col-lg-4 mb-2">
                    </div>
                    <div class="col-lg-4 mb-2 text-center">
                        <a href="#/duvida_zero_exame/?id_dz=<?=$_GET['id_dz']?>" class="btn btn-success"><i class="far fa-check-circle"></i> Iniciar</a>
                        <a href="#/duvida_zero/" class="btn btn-secondary"><i class="fas fa-arrow-left"></i> Voltar</a>
                    </div>
                    <div class="col-lg-4 mb-2">
                    </div>
                </div>
                <?php } ?>                 
            </div>
        </div>
    </div>

</div>