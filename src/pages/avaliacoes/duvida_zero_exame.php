<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require_once('../../../vendor/autoload.php');

session_start();

use Plataforma_CV\Classes\Duvida_zero;

$objDZ          = new Duvida_zero();

$getDZAluno     = $objDZ->getDZAluno($_SESSION['user_id'],$_GET['id_dz']);
$getDZ          = $objDZ->getDZInfo($_GET['id_dz']);

$jsonQuestoes   = file_get_contents('../../json/dz_'.$_GET['id_dz'].'.json');
$questoes       = json_decode($jsonQuestoes);
$qtdQuestoes    = count((array) $questoes);

?>

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><?=$getDZ->titulo?></h1>

    <?php if(!$getDZAluno){ ?>
        <div class="fixme" style="font-weight:bold;z-index:99999;background:white">Tempo restante de prova: <span id="time">Carregando...</span></div>
    <?php }?>
</div>

<!-- Content Row -->
<div class="row">
    
    <!-- Content Column -->
    <div class="col-lg-12 mb-4">
        <!-- Project Card Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <?php if($getDZAluno){ ?>
                <h6 class="m-0 font-weight-bold text-primary">Prova finalizada</h6>
                    <?php } else {?>
                <h6 class="m-0 font-weight-bold text-primary">Questões</h6>
                <?php } ?>
            </div>
            <div class="card-body">
                <?php if($getDZAluno){ ?>
                
                    <div class="row">
                        <div class="col-lg-12 mb-4">
                            <?="Você já realizou esta prova! Qualquer dúvida, entre em contato com a coordenação."?>
                        </div>
                    </div>
                    
                <?php } else{

                    //ADICIONA UM REGISTRO DE PROVA DO ALUNO EM duvida_zero_aluno
                    $arrayNewDzAluno = Array(
                        'id_user'   => $_SESSION['user_id'],
                        'id_dz'     => $_GET['id_dz'],
                    );

                    $objDZ->createNewDZAluno($arrayNewDzAluno);
                
                    foreach($questoes as $x => $p){ ?>

                        <div class="row">
                            <div class="col-lg-12 mb-4">
                                <?=$p->descricao?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 mb-4">
                                <?php if(trim($p->resposta_1) != ''){ ?>
                                <div class="form-check mb-1">
                                    <input class="form-check-input" type="radio" value="1" name="resposta_<?=$x?>" id="resposta_<?=$x?>">
                                    <label class="form-check-label" for="flexRadioDefault1">
                                        <?=$p->resposta_1?>
                                    </label>
                                </div>
                                <?php } if(trim($p->resposta_2) != ''){?>
                                <div class="form-check mb-1">
                                    <input class="form-check-input" type="radio" value="2" name="resposta_<?=$x?>" id="resposta_<?=$x?>">
                                    <label class="form-check-label" for="flexRadioDefault1">
                                        <?=$p->resposta_2?>
                                    </label>
                                </div>
                                <?php } if(trim($p->resposta_3) != ''){?>
                                <div class="form-check mb-1">
                                    <input class="form-check-input" type="radio" value="3" name="resposta_<?=$x?>" id="resposta_<?=$x?>">
                                    <label class="form-check-label" for="flexRadioDefault1">
                                        <?=$p->resposta_3?>
                                    </label>
                                </div>
                                <?php } if(trim($p->resposta_4) != ''){?>
                                <div class="form-check mb-1">
                                    <input class="form-check-input" type="radio" value="4" name="resposta_<?=$x?>" id="resposta_<?=$x?>">
                                    <label class="form-check-label" for="flexRadioDefault1">
                                        <?=$p->resposta_4?>
                                    </label>
                                </div>
                                <?php } if(trim($p->resposta_5) != ''){?>
                                <div class="form-check mb-1">
                                    <input class="form-check-input" type="radio" value="5" name="resposta_<?=$x?>" id="resposta_<?=$x?>">
                                    <label class="form-check-label" for="flexRadioDefault1">
                                        <?=$p->resposta_5?>
                                    </label>
                                </div>
                                <?php } ?>
                            </div>
                        </div>


                <?php } }?>

                <?php if(!$getDZAluno){ ?>
                    <br>
                    <div class="row">
                        <div class="col-lg-4 mb-2">
                        </div>
                        <div class="col-lg-4 mb-2 text-center">
                            <button id="entregar_dz" class="btn btn-success"><i class="far fa-check-circle"></i> Entregar</button>
                        </div>
                        <div class="col-lg-4 mb-2">
                        </div>
                    </div>
                <?php } ?>
                               
            </div>
        </div>
    </div>

</div>

<script>

//CÓDIGO QUE INSERE O TIMER DE TEMPO DA PROVA        
//9000 = 2HRS E MEIA

let display = document.querySelector('#time');
let timer = 9000, minutes, seconds;
let setIntervalTimer = setInterval(function() {
    minutes = parseInt(timer / 60, 10)
    seconds = parseInt(timer % 60, 10);

    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;

    display.textContent = minutes + ":" + seconds;

    if (--timer < 0) {
        entregar_dz(true);
        timer = null;
        clearInterval(setIntervalTimer);
    }
}, 1000);

//FIXAR TIMER NO TOPO DA PÁGINA
let fixmeTop = $('.fixme').offset().top;       // get initial position of the element

$(window).scroll(function() {                  // assign scroll event listener
    let currentScroll = $(window).scrollTop(); // get current position
    if (currentScroll >= fixmeTop) {           // apply position: fixed if you
        $('.fixme').css({                      // scroll to that element or below it
            position: 'fixed',
            top: '20px',
            right: '33px',
        });
    } 
    else {                                   // apply position: static
        $('.fixme').css({                      // if you scroll above it
            position: 'static'
        });
    }
});

$("#entregar_dz").on('click',function(){

    Swal.fire({
        title: 'Tem certeza que deseja entregar a prova?',
        type: "question",
        showCancelButton: true,
        confirmButtonText: 'Entregar',
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#17A673'
    }).then((result) => {
        if(result.value) {

            entregar_dz();
            timer = null;
            clearInterval(setIntervalTimer);
        }
    });

});

function entregar_dz(byEndTime = false){

    let id_dz       = "<?=$_GET['id_dz']?>";
    let user_id     = "<?=$_SESSION['user_id']?>";
    let qtdQuestoes = "<?=$qtdQuestoes?>";
    let array       = [];
    let jsonStr     = '{';

    let typeSwal    = 'success';
    let titleSwal   = 'Tudo certo!';    
    let bodySwal    = 'Sua prova foi enviada com sucesso.';
    if(byEndTime === true){
        typeSwal    = 'warning';
        titleSwal   = 'Atenção!';    
        bodySwal    = 'O tempo de prova acabou. Sua prova foi enviada com as questões respondidas.';
    }    

    for(let x=1;x<=qtdQuestoes;x++){

        let resposta = $('input[name=resposta_P'+x+']:checked').val();
        if(resposta === undefined){
            resposta = '';
        }

        jsonStr = jsonStr+'"P'+x+'":"'+resposta+'"';

        if(x<qtdQuestoes){
            jsonStr = jsonStr+',';
        }

    }

    jsonStr = jsonStr+'}';

    $.ajax({
        type: "POST",
        url: "src/api/duvida_zero.php",
        data: { 
            type: 'entregarDuvidaZero',
            id_dz: id_dz,
            user_id: user_id,
            questoes: jsonStr
        },
        beforeSend: function(){
            //$('#cover-spin').show();
        },
        success: function (res) {
                    
            //$('#cover-spin').hide();
            
            res = JSON.parse(res);

            if(res.status == 1){
                Swal.fire({
                    title: titleSwal,
                    html: bodySwal,
                    type: typeSwal,
                    onClose:function(){
                        //location.reload();
                        window.location.href = "#/duvida_zero/";
                    }
                });
            }
            else{
                Swal.fire({
                    title: 'Ops!',
                    html: "ocorreu um erro ao enviar a prova. Tente novamente ou contate o administrador",
                    type: "error"
                });
            }

        },
        error:function(xhr, status, error){
            let errorMessage = xhr.status + ': ' + xhr.statusText
            alert(errorMessage);
        }
    });

}

</script>