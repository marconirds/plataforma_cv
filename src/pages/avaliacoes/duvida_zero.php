<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require_once('../../../vendor/autoload.php');

session_start();

use Plataforma_CV\Classes\Duvida_zero;
use Plataforma_CV\Classes\AbstractClass;

$objDZ              = new Duvida_zero();
$objAbstract        = new AbstractClass();

$getDZ              = $objDZ->getDZemAberto();
$getDZAnteriores    = $objDZ->getDZAnteriores($_SESSION['user_id']);

if($_GET['id_dz']){
    $getRanking = $objDZ->getRanking($_GET['id_dz'],'aluno');
    $getDZAluno = $objDZ->getDZAluno($_SESSION['user_id'],$_GET['id_dz']);
}
?>

<script src="src/charts/media-turma.js"></script> 

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Dúvida Zero</h1>
    
    <?php if($getDZAnteriores){ ?>
        
    <div class="dropdown">
        <?php if($getDZAluno){ ?>
            <button id="voltar" class="btn btn-secondary"><i class="fas fa-arrow-left"></i> Voltar</button>
        <?php } ?>
        <button class="btn btn-success dropdown-toggle" id="dropdownProvasAnteriores" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Consultar provas realizadas</button>
        <div class="dropdown-menu animated--fade-in-up" aria-labelledby="dropdownFadeInUp">
            <?php foreach($getDZAnteriores as $dz){ $getDZInfo = $objDZ->getDZInfo($dz->id_dz); ?>   
                <button class="dropdown-item" id-dz="<?=$getDZInfo->id_dz?>"><?=$getDZInfo->titulo?></button>
            <?php } ?>    
        </div>
    </div>
    <?php } ?>

</div>

<?php if(!$getDZAluno){?>

<!-- Content Row -->
<div class="row">
    
    <!-- Content Column -->
    <div class="col-lg-12 mb-4">
        <!-- Project Card Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Disponíveis</h6>
            </div>
            <div class="card-body">
                
                <?php if($getDZ){ ?>
                        <ul>
                        <?php foreach($getDZ as $dz){ 
                            if($objDZ->verifyAlunoHasDZ($_SESSION['user_id'],$dz->id_dz)){ ?>
                                <li><?=mb_strtoupper($dz->titulo,"UTF-8")?> &nbsp;&nbsp;<span class="text-primary">Realizado&nbsp;<i class="fas fa-marker"></i></span></li>  
                            <?php } else{ ?>    
                            <li><?=mb_strtoupper($dz->titulo,"UTF-8")?> &nbsp;&nbsp;<a href="#/duvida_zero_iniciar/?id_dz=<?=$dz->id_dz?>" class="button-icon text-success">Iniciar&nbsp;<i class="far fa-check-circle"></i></a></li>  
                <?php } }?> </ul> <?php } else{ ?> <div>Nenhum dúvida zero disponível no momento.</div> <?php } ?> 
                              
            </div>
        </div>
    </div>

</div>

<?php } ?> 

<?php if($getDZAluno){ $getDZInfo = $objDZ->getDZInfo($_GET['id_dz']);?>

<div class="row">

    <div class="col-lg-6">
        <div class="card shadow mb-4">
            
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Seu Resultado / <?=$getDZInfo->titulo?></h6>
            </div>
            
            <div class="card-body">
                <div>Nota: <b><?=$getDZAluno->nota?></b></div>
                <div>Tempo (minutos): <b><?=$getDZAluno->tempo?></b></div>
                <div>Acertos: <b><?=$getDZAluno->acertos?></b></div>
                <div>Início: <b><?=$objAbstract->inverteData(substr($getDZAluno->date_add,0,10)).' '.substr($getDZAluno->date_add,11)?></b></div>
                <div>Fim: <b><?=($getDZAluno->date_end) ? $objAbstract->inverteData(substr($getDZAluno->date_end,0,10)).' '.substr($getDZAluno->date_end,11) : ''?></b></div>
                <div>Questões respondidas: <button class="button-icon text-success viewRespostas" id-dz-aluno="<?=$getDZAluno->id_dz_aluno?>" data-toggle="modal" data-target="#viewRespostas">Visualizar&nbsp;<i class="fas fa-search"></i></button></div>
            </div>
        </div>
    </div>

    <!-- <div class="col-lg-6">
        <div class="card shadow mb-4">
            
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Média geral / <?=$getDZInfo->titulo?></h6>
            </div>
            
            <div class="card-body">
                <div class="chart-area"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                    <canvas id="chartMediaTurma" width="916" height="100" style="display: block; height: 100px; width: 611px;" class="chartjs-render-monitor"></canvas>
                </div>
            </div>
        </div>
    </div> -->

</div>

<?php } ?> 

<?php if($getRanking){ $getDZInfo = $objDZ->getDZInfo($_GET['id_dz']);?>

<div class="row">

    <div class="col-lg-12">
        <div class="card shadow mb-4">
            
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Ranking Geral / <?=$getDZInfo->titulo?></h6>
            </div>
            
            <div class="card-body">
                <!--<div class="chart-area"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                    <canvas id="myBarChart" width="916" height="480" style="display: block; height: 320px; width: 611px;" class="chartjs-render-monitor"></canvas>
                </div>-->
                <table id="dataTableRanking">
                    <thead>
                        <tr>
                            <th class="text-center">Posição</th>
                            <th>Aluno</th>
                            <th class="text-center">Matrícula</th>
                            <th class="text-center">Turma</th>
                            <th class="text-center">Nota</th>
                            <th class="text-center">Tempo (Minutos)</th>
                        </tr>    
                    </thead>
                    <tbody>
                        <?php foreach($getRanking as $i => $rk){ $i = $i+1;?>
                            <tr>
                                <td class="text-center"><?=$i."º"?></td>
                                <td><?=mb_strtoupper($rk->user_nome,"UTF-8")?></td>
                                <td class="text-center"><?=$rk->user_login?></td>
                                <td class="text-center"><?=$rk->user_turma?></td>
                                <td class="text-center"><?=number_format($rk->nota,2,'.','')?></td>
                                <td class="text-center"><?=$rk->tempo?></td>
                            </tr>
                           
                        <?php } ?>
                                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div> 
<?php } ?> 
    
   <!-- <div class="col-xl-4 col-lg-5">
        <div class="card shadow mb-4">
            
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Insights</h6>
            </div>
            
            <div class="card-body">
                <div class="chart-pie pt-4 pb-2"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                    <canvas id="myPieChart" width="409" height="367" style="display: block; height: 245px; width: 273px;" class="chartjs-render-monitor"></canvas>
                </div>
                <div class="mt-4 text-center small">
                    <span class="mr-2">
                        <i class="fas fa-circle text-primary"></i> Direct
                    </span>
                    <span class="mr-2">
                        <i class="fas fa-circle text-success"></i> Social
                    </span>
                    <span class="mr-2">
                        <i class="fas fa-circle text-info"></i> Referral
                    </span>
                </div>
            </div>
        </div>
    </div>-->
</div>

<!-- viewRespostas Modal-->
<div class="modal fade" id="viewRespostas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Respostas</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" id="modalBodyRespostas">
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Voltar</button>
            </div>
        </div>
    </div>
</div>


<script>

new DataTable("#dataTableRanking", {
    searchable: false,
    bSort: false,
    fixedHeight: true,
    responsive: true,
    language: {
        searchPlaceholder: 'Buscar...',
        sSearch: '',
        lengthMenu: 'Exibir _MENU_ itens',
        info: 'Exibindo _PAGE_ de _PAGES_ páginas',
        zeroRecords: 'Nenhum registro encontrado.',
        infoFiltered: '(Filtro de _MAX_ registros)',
        thousands: '.',
        decimal:  ',',
        infoEmpty: '',
        paginate: {
            'first':      'Primeiro',
            'last':       'Último',
            'next':       'Próximo',
            'previous':   'Anterior'
        },
    },
    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
});

$(".dropdown-item").on('click', function(){

    let id_dz   = $(this).attr('id-dz');

    $(".container-fluid").empty();
    $(".container-fluid").append('<div id="cover-spin"><img src="assets/img/6.svg" style="height:110px;display:block;margin:auto;margin-top:250px;"><h3 class="text-center">Aguarde...</h3></div>');
    setTimeout(function(){ $(".container-fluid").load('src/pages/avaliacoes/duvida_zero.php?id_dz='+id_dz); }, 1000);

});

$("#voltar").on('click', function(){

    $(".container-fluid").empty();
    $(".container-fluid").append('<div id="cover-spin"><img src="assets/img/6.svg" style="height:110px;display:block;margin:auto;margin-top:250px;"><h3 class="text-center">Aguarde...</h3></div>');
    setTimeout(function(){ $(".container-fluid").load('src/pages/avaliacoes/duvida_zero.php'); }, 1000);

});

$(".viewRespostas").on('click',function(){

    $('#modalBodyRespostas').empty();

    let id_dz_aluno = $(this).attr('id-dz-aluno');

    $.ajax({
        type: "POST",
        url: "src/api/adm/adm_duvida_zero.php",
        data: { 
            type: 'viewRespostasDZAluno',
            id_dz_aluno: id_dz_aluno
        },
        beforeSend: function(){
        },
        success: function (res) {
            
            res = JSON.parse(res);

            let modalBody = '<div>';

            for(var i in res) {

                let textResposta = '';
                if(res[i]['resposta'] == res[i]['gabarito']){
                    textResposta = '<span class="text-success ml-2">Correto</span>';
                }
                else{
                    textResposta = '<span class="text-danger ml-2">Incorreto</span>';
                }

                 let numPergunta = i.substr(1);

                modalBody = modalBody+'<span><b class="text-black">Questão '+numPergunta+'</b> = Sua Resposta: <b>'+res[i]['resposta']+'</b> / Gabarito: <b>'+res[i]['gabarito']+'</b>'+textResposta+'</span><br>';
                
            }

            modalBody = modalBody+'</div>';

            $('#modalBodyRespostas').append(modalBody);

        },
        error:function(xhr, status, error){
            let errorMessage = xhr.status + ': ' + xhr.statusText
            alert(errorMessage);
        }
    });

});

</script>