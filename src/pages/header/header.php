<?php 
session_start();
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

/***** VERIFICA SE HÁ USUÁRIO LOGADO *****/
if (!isset ($_SESSION["user_nome"]) == true ) {

	unset($_SESSION['user_id']);
    unset($_SESSION['user_matricula']);
    unset($_SESSION['user_nome']);
	unset($_SESSION['user_tipo']);
    unset($_SESSION['user_turma']);
  	session_destroy();
	header('Location:index.php');
}
else {

	$time 		= $_SESSION['time'];
	$limite 	= $_SESSION['limite'];
	$segundos 	= time()-$time;

	if($segundos > $limite){

		$pageRedirect = "index.php";
		if($_SESSION['user_tipo'] == 0){
			$pageRedirect = "adm.php";
		}

        unset($_SESSION['user_id']);
        unset($_SESSION['user_matricula']);
        unset($_SESSION['user_nome']);
        unset($_SESSION['user_turma']);
    	session_destroy();
    	echo "<script>alert('Sessão Esgotada por Tempo de Inatividade!')</script>";
  		echo "<script>window.location.href = '".$pageRedirect."'</script>";
	}
	else{
    	$_SESSION['time'] = time();
	}

}
?>