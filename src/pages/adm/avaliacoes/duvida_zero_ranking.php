<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require_once('../../../../vendor/autoload.php');

use Plataforma_CV\Classes\Duvida_zero;
use Plataforma_CV\Classes\AbstractClass;

$objDZ          = new Duvida_zero();
$objAbstract    = new AbstractClass();

$getDZInfo      = $objDZ->getDZInfo($_GET['id_dz']);
$getRanking     = $objDZ->getRanking($_GET['id_dz']);
?>

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><?=$getDZInfo->titulo?></h1>
    <div class="dropdown">
        <button id="download" id-dz="<?=$_GET['id_dz']?>" class="btn btn-success"><i class="fas fa-download"></i> Download</button>
    </div>
</div>

<!-- Content Row -->
<div class="row">
    
    <!-- Content Column -->
    <div class="col-lg-12 mb-4">
        <!-- Project Card -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Ranking</h6>
            </div>
            <div class="card-body">
                <table id="datatablesSimple">
                    <thead>
                        <tr>
                            <th class="text-center">Posição</th>
                            <th>Aluno</th>
                            <th class="text-center">Matrícula</th>
                            <th class="text-center">Turma</th>
                            <th class="text-center">Nota</th>
                            <th class="text-center">Tempo (Min.)</th>
                            <th class="text-center">Acertos</th>
                            <th class="text-center">Início</th>
                            <th class="text-center">Término</th>
                            <th class="text-center">Ações</th>
                        </tr>    
                    </thead>
                    <tbody>
                    <?php if($getRanking){ ?>
                        <?php foreach($getRanking as $i => $rk){ $i = $i+1;?>
                            <tr>
                                <td class="text-center"><?=$i."º"?></td>
                                <td><?=mb_strtoupper($rk->user_nome,"UTF-8")?></td>
                                <td class="text-center"><?=$rk->user_login?></td>
                                <td class="text-center"><?=$rk->user_turma?></td>
                                <td class="text-center"><?=number_format($rk->nota,2,'.','')?></td>
                                <td class="text-center"><?=$rk->tempo?></td>
                                <td class="text-center"><?=$rk->acertos?></td>
                                <td class="text-center"><?=$objAbstract->inverteData(substr($rk->date_add,0,10)).' '.substr($rk->date_add,11)?></td>
                                <td class="text-center"><?=($rk->date_end) ? $objAbstract->inverteData(substr($rk->date_end,0,10)).' '.substr($rk->date_end,11) : ''?></td>
                                <td class="text-center">
                                    <button class="btn btn-datatable btn-icon btn-transparent-dark deleteDZAluno" data-tt="tooltip" data-placement="bottom" title="Cancelar prova" id-dz-aluno="<?=$rk->id_dz_aluno?>"><i class="far fa-times-circle text-danger"></i></button>
                                    <button class="btn btn-datatable btn-icon btn-transparent-dark viewRespostas" data-toggle="modal" data-target="#viewRespostas" data-tt="tooltip" data-placement="bottom" title="Respostas" id-dz-aluno="<?=$rk->id_dz_aluno?>"><i class="fas fa-desktop text-success"></i></button>
                                </td>
                            </tr>
                           
                    <?php } }?>
                                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-lg-4 mb-2">
    </div>
    <div class="col-lg-4 mb-2 text-center">
        <a href="#/adm_ger_duvida_zero/" class="btn btn-secondary"><i class="fas fa-arrow-left"></i> Voltar</a>
    </div>
    <div class="col-lg-4 mb-2">
    </div>
</div>

<!-- viewRespostas Modal-->
<div class="modal fade" id="viewRespostas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Respostas</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" id="modalBodyRespostas">
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Voltar</button>
            </div>
        </div>
    </div>
</div>

<script>

$(document).ready(function() {

    $(function () {
        $('[data-tt="tooltip"]').tooltip();
    });

    const dataTable = new DataTable("#datatablesSimple", {
	    searchable: false,
	    bSort: false,
	    fixedHeight: true,
        responsive: true,
        language: {
            searchPlaceholder: 'Buscar...',
            sSearch: '',
            lengthMenu: 'Exibir _MENU_ itens',
            info: 'Exibindo _PAGE_ de _PAGES_ páginas',
            zeroRecords: 'Nenhum registro encontrado.',
            infoFiltered: '(Filtro de _MAX_ registros)',
            thousands: '.',
            decimal:  ',',
            infoEmpty: '',
            paginate: {
                'first':      'Primeiro',
                'last':       'Último',
                'next':       'Próximo',
                'previous':   'Anterior'
            },
        },
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    });

});

$(".deleteDZAluno").on('click',function(){

    let id_dz_aluno = $(this).attr('id-dz-aluno');
   
    Swal.fire({
        title: 'Tem certeza que deseja deletar a prova do aluno?',
        type: "question",
        showCancelButton: true,
        confirmButtonText: 'Deletar',
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#E74A3B'
    }).then((result) => {
        if(result.value) {

            $.ajax({
                type: "POST",
                url: "src/api/adm/adm_duvida_zero.php",
                data: { 
                    type: 'deleteDZAluno',
                    id_dz_aluno: id_dz_aluno
                },
                beforeSend: function(){
                    
                },
                success: function (res) {
            
                    res = JSON.parse(res);

                    if(res.status == 1){
                        Swal.fire({
                            title: 'Tudo certo!',
                            html: 'A prova foi apagada com sucesso.',
                            type: "success",
                            onClose:function(){
                                location.reload();
                            }
                        });
                    }
                    else{
                        Swal.fire({
                            title: 'Ops!',
                            html: "ocorreu um erro. Tente novamente.",
                            type: "error"
                        });
                    }

                },
                error:function(xhr, status, error){
                    let errorMessage = xhr.status + ': ' + xhr.statusText
                    alert(errorMessage);
                }
            });

        }
    });

});

$(".viewRespostas").on('click',function(){

    $('#modalBodyRespostas').empty();

    let id_dz_aluno = $(this).attr('id-dz-aluno');

    $.ajax({
        type: "POST",
        url: "src/api/adm/adm_duvida_zero.php",
        data: { 
            type: 'viewRespostasDZAluno',
            id_dz_aluno: id_dz_aluno
        },
        beforeSend: function(){
        },
        success: function (res) {
            
            res = JSON.parse(res);

            let modalBody = '<div>';

            for(var i in res) {

                let textResposta = '';
                if(res[i]['resposta'] == res[i]['gabarito']){
                    textResposta = '<span class="text-success ml-2">Correto</span>';
                }
                else{
                    textResposta = '<span class="text-danger ml-2">Incorreto</span>';
                }

                let numPergunta = i.substr(1);

                modalBody = modalBody+'<span><b class="text-black">Questão '+numPergunta+'</b> = Resposta aluno: <b>'+res[i]['resposta']+'</b> / Gabarito: <b>'+res[i]['gabarito']+'</b>'+textResposta+'</span><br>';

            }

            modalBody = modalBody+'</div>';

            $('#modalBodyRespostas').append(modalBody);

        },
        error:function(xhr, status, error){
            let errorMessage = xhr.status + ': ' + xhr.statusText
            alert(errorMessage);
        }
    });

});

$("#download").on('click',function(){

    let id_dz = $(this).attr('id-dz');
    window.location.href = "src/pages/adm/avaliacoes/getCsvFileRanking.php?id_dz="+id_dz;

});

</script>