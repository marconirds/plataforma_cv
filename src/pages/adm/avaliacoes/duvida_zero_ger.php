<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require_once('../../../../vendor/autoload.php');

use Plataforma_CV\Classes\Duvida_zero;
use Plataforma_CV\Classes\AbstractClass;

$objDZ          = new Duvida_zero();
$objAbstract    = new AbstractClass();

$getDZ  = $objDZ->getAllDZ();

?>

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Dúvida Zero</h1>
</div>

<!-- Content Row -->
<div class="row">
    
    <!-- Content Column -->
    <div class="col-lg-12 mb-4">
        <!-- Project Card -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Gerenciar</h6>
            </div>
            <div class="card-body">
                <table id="datatablesSimple">
                    <thead>
                        <tr>
                            <th>Título</th>
                            <th class="text-center">Criação</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Mostrar ranking</th>
                            <th class="text-center">Ações</th>
                        </tr>    
                    </thead>
                    <tbody>
                    <?php if($getDZ){ ?>
                        <?php foreach($getDZ as $dz){ ?>
                            <tr>
                                <td><?=mb_strtoupper($dz->titulo,"UTF-8")?></td>
                                <td class="text-center"><?=$objAbstract->inverteData(substr($dz->date_add,0,10)).' '.substr($dz->date_add,11)?></td>
                                <td class="text-center">
                                    <div class="custom-control custom-switch custom-switch-md">
                                        <input type="checkbox" class="custom-control-input status-dz" id="status-dz-<?=$dz->id_dz?>" id-dz="<?=$dz->id_dz?>" <?=($dz->status == 1 ? 'checked' : '')?>>
                                        <label class="custom-control-label" for="status-dz-<?=$dz->id_dz?>"></label>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <div class="custom-control custom-switch custom-switch-md">
                                        <input type="checkbox" class="custom-control-input mostrar-ranking-dz" id="mostrar-ranking-dz-<?=$dz->id_dz?>" id-dz="<?=$dz->id_dz?>" <?=($dz->mostrar_ranking == 1 ? 'checked' : '')?>>
                                        <label class="custom-control-label" for="mostrar-ranking-dz-<?=$dz->id_dz?>"></label>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <button class="btn btn-datatable btn-icon btn-transparent-dark" data-toggle="tooltip" data-placement="bottom" title="Editar DZ" onclick="location.href = '#/adm_editar_duvida_zero/?dz_id=<?=$dz->id_dz?>';"><i class="far fa-edit text-info"></i></button>
                                    <button class="btn btn-datatable btn-icon btn-transparent-dark deleteDZ" data-toggle="tooltip" data-placement="bottom" title="Apagar DZ" dz-id="<?=$dz->id_dz?>"><i class="far fa-trash-alt text-danger"></i></button>
                                    <button class="btn btn-datatable btn-icon btn-transparent-dark" data-toggle="tooltip" data-placement="bottom" title="Ranking" onclick="location.href = '#/adm_duvida_zero_ranking/?dz_id=<?=$dz->id_dz?>';"><i class="far fa-list-alt text-success"></i></button>
                                </td>
                            </tr>
                           
                    <?php } ?><?php } else{ ?> <div>Nenhum dúvida zero cadastrado no momento.</div> <?php } ?>
                                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<script>

$(document).ready(function() {

    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    const dataTable = new DataTable("#datatablesSimple", {
	    searchable: false,
	    fixedHeight: true,
        responsive: true,
        language: {
            searchPlaceholder: 'Buscar...',
            sSearch: '',
            lengthMenu: 'Exibir _MENU_ itens',
            info: 'Exibindo _PAGE_ de _PAGES_ páginas',
            zeroRecords: 'Nenhum registro encontrado.',
            infoFiltered: '(Filtro de _MAX_ registros)',
            thousands: '.',
            decimal:  ',',
            infoEmpty: '',
            paginate: {
                'first':      'Primeiro',
                'last':       'Último',
                'next':       'Próximo',
                'previous':   'Anterior'
            },
        },
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
    });

});

$(".status-dz").on('change',function(){

    let id_dz   = $(this).attr('id-dz');
    let status  = $(this).is(':checked');
    if(status === true){
        status = 1;
    }
    else{
        status = 0;
    }

    $.ajax({
        type: "POST",
        url: "src/api/adm/adm_duvida_zero.php",
        data: { 
            type: 'enableDisableStatusDZ', 
            id_dz: id_dz,
            status: status
        },
        beforeSend: function(){
            $('#cover-spin').show();
        },
        success: function (res) {
            
            res = JSON.parse(res);

            if(res.status == 1){
                Swal.fire({
                    title: 'Tudo certo!',
                    html: 'O Status do DZ foi atualizado.',
                    type: "success",
                    onClose:function(){
                    }
                });
            }
            else{
                Swal.fire({
                    title: 'Ops!',
                    html: "ocorreu um erro. Tente novamente",
                    type: "error"
                });
            }

            $('#cover-spin').hide();
        },
        error:function(xhr, status, error){
            let errorMessage = xhr.status + ': ' + xhr.statusText
            alert(errorMessage);
        }
    });

});

$(".mostrar-ranking-dz").on('change',function(){

    let id_dz   = $(this).attr('id-dz');
    let status  = $(this).is(':checked');
    if(status === true){
        status = 1;
    }
    else{
        status = 0;
    }

    $.ajax({
        type: "POST",
        url: "src/api/adm/adm_duvida_zero.php",
        data: { 
            type: 'enableDisableRankingDZ', 
            id_dz: id_dz,
            status: status
        },
        beforeSend: function(){
            $('#cover-spin').show();
        },
        success: function (res) {
            
            res = JSON.parse(res);

            if(res.status == 1){
                Swal.fire({
                    title: 'Tudo certo!',
                    html: 'A visualização do ranking foi atualizada.',
                    type: "success",
                    onClose:function(){
                    }
                });
            }
            else{
                Swal.fire({
                    title: 'Ops!',
                    html: "ocorreu um erro. Tente novamente",
                    type: "error"
                });
            }

            $('#cover-spin').hide();
        },
        error:function(xhr, status, error){
            let errorMessage = xhr.status + ': ' + xhr.statusText
            alert(errorMessage);
        }
    });

});

$(".deleteDZ").on('click',function(){

    let id_dz = $(this).attr('dz-id');
   
    Swal.fire({
        title: 'Tem certeza que deseja deletar o DZ?',
        type: "question",
        showCancelButton: true,
        confirmButtonText: 'Deletar',
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#E74A3B'
    }).then((result) => {
        if(result.value) {

            $.ajax({
                type: "POST",
                url: "src/api/adm/adm_duvida_zero.php",
                data: { 
                    type: 'deleteDZ',
                    id_dz: id_dz
                },
                beforeSend: function(){
                    
                },
                success: function (res) {
            
                    res = JSON.parse(res);

                    if(res.status == 1){
                        Swal.fire({
                            title: 'Tudo certo!',
                            html: 'O DZ foi apagado com sucesso.',
                            type: "success",
                            onClose:function(){
                                location.reload();
                            }
                        });
                    }
                    else{
                        Swal.fire({
                            title: 'Ops!',
                            html: "ocorreu um erro. Tente novamente.",
                            type: "error"
                        });
                    }

                },
                error:function(xhr, status, error){
                    let errorMessage = xhr.status + ': ' + xhr.statusText
                    alert(errorMessage);
                }
            });

        }
    });

});

</script>