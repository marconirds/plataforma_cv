<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once('../../../../vendor/autoload.php');

sleep(1);

use Plataforma_CV\Classes\Duvida_zero;
use Plataforma_CV\Classes\AbstractClass;

$objDZ          = new Duvida_zero();
$objAbstract    = new AbstractClass();

$getRanking     = $objDZ->getRanking($_GET['id_dz']);

$fp = fopen(getcwd().'/fileOutput.csv', 'w');

fputs($fp, $bom = chr(0xEF) . chr(0xBB) . chr(0xBF));

$header = 'POSICAO'.';'.'ALUNO'.';'.'MATRICULA'.';'.'TURMA'.';'.'NOTA'.';'.'TEMPO (MIN)'.';'.'ACERTOS'.';'.'INICIO'.';'.'TERMINO';
fwrite($fp, $header."\r\n");

foreach($getRanking as $i => $rk) {

    $i = $i+1;

    if($rk->date_end){
        $rk->date_end = $objAbstract->inverteData(substr($rk->date_end,0,10)).' '.substr($rk->date_end,11);
    }
    else{
        $rk->date_end = '';
    }

    $contentLine = $i."º".';'.mb_strtoupper($rk->user_nome,"UTF-8").';'.$rk->user_login.';'.$rk->user_turma.';'.number_format($rk->nota,2,'.','').';'.$rk->tempo.';'.$rk->acertos.';'.$objAbstract->inverteData(substr($rk->date_add,0,10)).' '.substr($rk->date_add,11).';'.$rk->date_end;

    fwrite($fp, $contentLine."\r\n");
}

    $fileName = 'Ranking_DZ.csv';
    $Nomeantigo = getcwd().'/fileOutput.csv';

    $read = file_get_contents($Nomeantigo);

    $download = $read;header('Content-type: text/plain');header("Content-Disposition: attachment; filename=\"" . $fileName . "\"");ob_clean();
    echo "\xEF\xBB\xBF";
    echo $download;
    exit;