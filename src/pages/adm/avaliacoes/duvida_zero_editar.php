<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require_once('../../../../vendor/autoload.php');

use Plataforma_CV\Classes\Duvida_zero;

$objDZ          = new Duvida_zero();
$getDZ          = $objDZ->getDZInfo($_GET['id_dz']);
$jsonQuestoes   = file_get_contents('../../../json/dz_'.$_GET['id_dz'].'.json');
$questoes       = json_decode($jsonQuestoes);
?>

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Dúvida Zero</h1>
    <!--<button class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</button>-->

</div>

<!-- Content Row -->
<div class="row">
    
    <!-- Content Column -->
    <div class="col-lg-12 mb-4">
        <!-- Project Card Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Editar</h6>
            </div>
            <div class="card-body">
                
                <div class="row">
                    <div class="col-lg-12 mb-4">
                        <div class="mb-3">
                            <label for="exampleFormControlInput1">Título:</label>
                            <input class="form-control" id="titulo-dz" type="text" placeholder="Insira uma título..." value="<?=$getDZ->titulo?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 mb-4">
                        
                        <ul class="nav nav-tabs" id="tabs-dz" role="tablist">
                            <?php for($x=1;$x<=60;$x++){ $active = ''; if($x==1){$active = 'active';}?>
                                <li class="nav-item" role="presentation">
                                    <a class="header-p nav-link <?=$active?>" id="tab-p-<?=$x?>" data-toggle="tab" href="#content-tab-<?=$x?>" role="tab" aria-controls="home" aria-selected="true">P<?=$x?></a>
                                </li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content" id="tab-content-dz">
                            <?php for($x=1;$x<=60;$x++){ $active = ''; if($x==1){$active = 'active';} $xNomeAtual = 'P'.$x;?>
                                <div class="tab-pane fade show <?=$active?>" id="content-tab-<?=$x?>" role="tabpanel" aria-labelledby="home-tab">
                            
                                    <div class="row">
                                        <div class="col-lg-12 mt-4">
                                            <div class="form-group">
                                                <label for="exampleFormControlTextarea1">Descrição P<?=$x?>:</label>
                                                <textarea class="form-control editor" id="descricao_P<?=$x?>" rows="3"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-6 mb-2">
                                            <div class="mb-3"><label for="exampleFormControlInput1">Resposta 1:</label><input class="form-control" id="resposta_1_P<?=$x?>" type="text" value="<?=$questoes->$xNomeAtual->resposta_1?>"></div>
                                        </div>
                                        <div class="col-lg-6 mb-2">
                                            <div class="mb-3"><label for="exampleFormControlInput1">Resposta 2:</label><input class="form-control" id="resposta_2_P<?=$x?>" type="text" value="<?=$questoes->$xNomeAtual->resposta_2?>"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 mb-2">
                                            <div class="mb-3"><label for="exampleFormControlInput1">Resposta 3:</label><input class="form-control" id="resposta_3_P<?=$x?>" type="text" value="<?=$questoes->$xNomeAtual->resposta_3?>"></div>
                                        </div>
                                         <div class="col-lg-6 mb-2">
                                            <div class="mb-3"><label for="exampleFormControlInput1">Resposta 4:</label><input class="form-control" id="resposta_4_P<?=$x?>" type="text" value="<?=$questoes->$xNomeAtual->resposta_4?>"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 mb-2">
                                            <div class="mb-3"><label for="exampleFormControlInput1">Resposta 5:</label><input class="form-control" id="resposta_5_P<?=$x?>" type="text" value="<?=$questoes->$xNomeAtual->resposta_5?>"></div>
                                        </div>
                                        <div class="col-lg-6 mb-2">
                                        <div class="mb-2"><label for="exampleFormControlInput1">Gabarito:</label></div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" value="1" name="gabarito_P<?=$x?>" id="gabarito_P<?=$x?>" <?= ($questoes->$xNomeAtual->gabarito == "1") ? "checked" : null; ?>>
                                                <label class="form-check-label" for="flexRadioDefault1">
                                                    1
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" value="2" name="gabarito_P<?=$x?>" id="gabarito_P<?=$x?>" <?= ($questoes->$xNomeAtual->gabarito == "2") ? "checked" : null; ?>>
                                                <label class="form-check-label" for="flexRadioDefault1">
                                                    2
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" value="3" name="gabarito_P<?=$x?>" id="gabarito_P<?=$x?>" <?= ($questoes->$xNomeAtual->gabarito == "3") ? "checked" : null; ?>>
                                                <label class="form-check-label" for="flexRadioDefault1">
                                                    3
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" value="4" name="gabarito_P<?=$x?>" id="gabarito_P<?=$x?>" <?= ($questoes->$xNomeAtual->gabarito == "4") ? "checked" : null; ?>>
                                                <label class="form-check-label" for="flexRadioDefault1">
                                                    4
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" value="5" name="gabarito_P<?=$x?>" id="gabarito_P<?=$x?>" <?= ($questoes->$xNomeAtual->gabarito == "5") ? "checked" : null; ?>>
                                                <label class="form-check-label" for="flexRadioDefault1">
                                                    5
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-3 mb-2">
                                        </div>
                                        <div class="col-lg-6 mb-2">
                                            <div class="mb-3"><label for="exampleFormControlInput1">Resolução:</label><input class="form-control" id="resolucao_P<?=$x?>" type="text" placeholder="Link do vídeo..." value="<?=$questoes->$xNomeAtual->resolucao?>"></div>
                                        </div>
                                        <div class="col-lg-3 mb-2">
                                        </div>
                                    </div>
                                
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 mb-2">
                    </div>
                    <div class="col-lg-4 mb-2 text-center">
                        <button id="send_atualizar_dz" class="btn btn-success"><i class="fas fa-redo"></i> Atualizar</button>
                        <a href="#/adm_ger_duvida_zero/" class="btn btn-secondary"><i class="fas fa-arrow-left"></i> Voltar</a>
                    </div>
                    <div class="col-lg-4 mb-2">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script>

tinymce.init({
    selector: 'textarea.editor',
    language: 'pt_BR',
    remove_linebreaks : true,
    entity_encoding : 'raw',
    editor_encoding : 'raw',
    menubar: false,
    plugins: 'image',
    //images_upload_url: './src/pages/adm/avaliacoes/postAcceptor.php',
    //images_reuse_filename: true,
    //images_upload_base_path: 'assets/img',
    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    images_upload_url : 'upload_image.php',
    automatic_uploads : false,
    convert_urls: false,

    images_upload_handler: function (blobInfo, success, failure) {
        
        let xhr, formData;
      
        xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open('POST', 'upload_image.php');
        xhr.setRequestHeader("Content-Type", "multipart/form-data");
      
        xhr.onload = function() {

            let json;
        
            if (xhr.status != 200) {
                failure('HTTP Error: ' + xhr.status);
                return;
            }
        
            json = JSON.parse(xhr.responseText);
            
                if (!json || typeof json.location != 'string') {
                    failure('Invalid JSON: ' + xhr.responseText);
                    return;
                }
        
            success(json.location);
        };
      
        formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());
      
        xhr.send(formData);
    
    },
		
});

//PREENCHE OS TINY EDITOR COM AS DESCRIÇÕES DAS QUESTÕES DEPOIS DE 8 SEGUNDOS
setTimeout(function(){

    let id_dz = "<?=$_GET['id_dz']?>";

    $.ajax({
        type: "POST",
        url: "src/api/adm/adm_duvida_zero.php",
        data: { 
            type: 'getJsonFromFile', 
            id_dz: id_dz
        },
        beforeSend: function(){
            $('#cover-spin').show();
        },
        success: function (res) {
            
            res = JSON.parse(res);

            const keys = Object.keys(res);

            keys.forEach((key, index) => {
                //console.log(`${key}: ${res[key]['descricao']}`);
                tinymce.get('descricao_'+`${key}`).setContent(`${res[key]['descricao']}`);
            });

        },
        error:function(xhr, status, error){
            let errorMessage = xhr.status + ': ' + xhr.statusText
            alert(errorMessage);
        }
    });
    
}, 8000);

$("#send_atualizar_dz").on('click',function(){

    let id_dz   = "<?=$_GET['id_dz']?>";
    let titulo  = $('#titulo-dz').val();
    
    let array = [];
    let jsonStr   = "{";

    for(let x=1;x<=60;x++){

        let gabarito = $('input[name=gabarito_P'+x+']:checked').val();
        if(gabarito === undefined){
            gabarito = '';
        }

        let textTiny = tinymce.get('descricao_P'+x).getContent();
            textTiny = textTiny.replace(/"/g, '\'');

        jsonStr = jsonStr+'"P'+x+'":{"descricao":"'+textTiny+'",';
        jsonStr = jsonStr+'"resposta_1":"'+$('#resposta_1_P'+x).val()+'",';
        jsonStr = jsonStr+'"resposta_2":"'+$('#resposta_2_P'+x).val()+'",';
        jsonStr = jsonStr+'"resposta_3":"'+$('#resposta_3_P'+x).val()+'",';
        jsonStr = jsonStr+'"resposta_4":"'+$('#resposta_4_P'+x).val()+'",';
        jsonStr = jsonStr+'"resposta_5":"'+$('#resposta_5_P'+x).val()+'",';
        jsonStr = jsonStr+'"gabarito":"'+gabarito+'",';
        jsonStr = jsonStr+'"resolucao":"'+$('#resolucao_P'+x).val()+'"}';

        if(x<60){
            jsonStr = jsonStr+",";
        }

    }
    
    jsonStr     = jsonStr+"}";

    jsonStr     = jsonStr.replace(/(\r\n|\n|\r)/gm, '');
    
    $.ajax({
        type: "POST",
        url: "src/api/adm/adm_duvida_zero.php",
        data: { 
            type: 'updateDuvidaZero',
            id_dz: id_dz,
            titulo: titulo,
            questoes: jsonStr
        },
        beforeSend: function(){
            $('#cover-spin').show();
        },
        success: function (res) {

            res = JSON.parse(res);

            if(res.status == 1){
                Swal.fire({
                    title: 'Tudo certo!',
                    html: 'O Dúvida Zero foi atualizado com sucesso.',
                    type: "success",
                    onClose:function(){
                        location.reload();
                    }
                });
            }
            else{
                Swal.fire({
                    title: 'Ops!',
                    html: "ocorreu um erro. Tente novamente",
                    type: "error"
                });
            }

            $('#cover-spin').hide();
        },
        error:function(xhr, status, error){
            let errorMessage = xhr.status + ': ' + xhr.statusText
            alert(errorMessage);
        }
    });

});    

</script>