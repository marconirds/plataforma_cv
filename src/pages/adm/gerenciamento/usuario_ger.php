<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require_once('../../../../vendor/autoload.php');

use Plataforma_CV\Classes\Gerenciamento;
use Plataforma_CV\Classes\AbstractClass;

$objGer         = new Gerenciamento();
$objAbstract    = new AbstractClass();

$getAllUser     = $objGer->getAllUser();
?>

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Usuários</h1>
</div>

<!-- Content Row -->
<div class="row">
    
    <!-- Content Column -->
    <div class="col-lg-12 mb-4">
        <!-- Project Card -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Gerenciar</h6>
            </div>
            <div class="card-body">
                <table id="datatablesSimple">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th class="text-center">Tipo</th>
                            <th class="text-center">Login</th>
                            <th class="text-center">Turma</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Ações</th>
                        </tr>    
                    </thead>
                    <tbody>
                    <?php if($getAllUser){ ?>
                        <?php foreach($getAllUser as $user){ ?>
                            <tr>
                                <td><?=$user->user_nome?></td>
                                <td class="text-center"><?=($user->user_tipo == 1) ? 'Aluno' : 'Não aluno'?></td>
                                <td class="text-center"><?=$user->user_login?></td>
                                <td class="text-center"><?=$user->user_turma?></td>
                                <td class="text-center"><?=$user->user_email?></td>
                                <td class="text-center">
                                    <div class="custom-control custom-switch custom-switch-md">
                                        <input type="checkbox" class="custom-control-input status-user" id-user="<?=$user->user_id?>" id="status-user-<?=$user->user_id?>" <?=($user->user_status == 1 ? 'checked' : '')?>>
                                        <label class="custom-control-label" for="status-user-<?=$user->user_id?>"></label>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <button class="btn btn-datatable btn-icon btn-transparent-dark" data-toggle="tooltip" data-placement="bottom" title="Editar usuário" onclick="location.href = '#/adm_editar_usuario/?user_id=<?=$user->user_id?>';"><i class="far fa-edit text-info"></i></button>
                                    <button class="btn btn-datatable btn-icon btn-transparent-dark deletarUser" data-toggle="tooltip" data-placement="bottom" title="Deletar usuário" user-id="<?=$user->user_id?>"><i class="far fa-trash-alt text-danger"></i></button>
                                </td>
                            </tr>
                           
                    <?php } ?><?php } else{ ?> <div>Nenhum dúvida zero cadastrado no momento.</div> <?php } ?>
                                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<script>

$(document).ready(function() {

    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    const dataTable = new DataTable("#datatablesSimple", {
	    searchable: false,
	    fixedHeight: true,
        responsive: true,
        language: {
            searchPlaceholder: 'Buscar...',
            sSearch: '',
            lengthMenu: 'Exibir _MENU_ itens',
            info: 'Exibindo _PAGE_ de _PAGES_ páginas',
            zeroRecords: 'Nenhum registro encontrado.',
            infoFiltered: '(Filtro de _MAX_ registros)',
            thousands: '.',
            decimal:  ',',
            infoEmpty: '',
            paginate: {
                'first':      'Primeiro',
                'last':       'Último',
                'next':       'Próximo',
                'previous':   'Anterior'
            },
        },
        "lengthMenu": [[10, 25, 50,-1], [10, 25, 50,"Todos"]],
    });

});

$(".status-user").on('change',function(){

    let id_user = $(this).attr('id-user');
    let status  = $(this).is(':checked');
    if(status === true){
        status = 1;
    }
    else{
        status = 0;
    }

    $.ajax({
        type: "POST",
        url: "src/api/adm/adm_gerenciamento.php",
        data: { 
            type: 'enableDisableStatusUser', 
            id_user: id_user,
            status: status
        },
        beforeSend: function(){
            $('#cover-spin').show();
        },
        success: function (res) {
            
            res = JSON.parse(res);

            if(res.status == 1){
                Swal.fire({
                    title: 'Tudo certo!',
                    html: 'O Status do usuário foi atualizado.',
                    type: "success",
                    onClose:function(){
                    }
                });
            }
            else{
                Swal.fire({
                    title: 'Ops!',
                    html: "ocorreu um erro. Tente novamente",
                    type: "error"
                });
            }

            $('#cover-spin').hide();
        },
        error:function(xhr, status, error){
            let errorMessage = xhr.status + ': ' + xhr.statusText
            alert(errorMessage);
        }
    });

});

$(".deletarUser").on('click',function(){

    let user_id = $(this).attr('user-id');

    Swal.fire({
        title: 'Tem certeza que deseja deletar o usuário?',
        type: "question",
        showCancelButton: true,
        confirmButtonText: 'Deletar',
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#E74A3B'
    }).then((result) => {
        if(result.value) {

            $.ajax({
                type: "POST",
                url: "src/api/adm/adm_gerenciamento.php",
                data: { 
                    type: 'deleteUser',
                    user_id: user_id
                },
                beforeSend: function(){
                    
                },
                success: function (res) {
            
                    res = JSON.parse(res);

                    if(res.status == 1){
                        Swal.fire({
                            title: 'Tudo certo!',
                            html: 'O usuário foi apagado com sucesso.',
                            type: "success",
                            onClose:function(){
                                location.reload();
                            }
                        });
                    }
                    else{
                        Swal.fire({
                            title: 'Ops!',
                            html: "ocorreu um erro. Tente novamente.",
                            type: "error"
                        });
                    }

                },
                error:function(xhr, status, error){
                    let errorMessage = xhr.status + ': ' + xhr.statusText
                    alert(errorMessage);
                }
            });

        }
    });

});





</script>