<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require_once('../../../../vendor/autoload.php');

use Plataforma_CV\Classes\Gerenciamento;

$objGer         = new Gerenciamento();
$getUserInfo     = $objGer->getUserInfo($_GET['user_id']);

?>

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Usuários</h1>
    <!--<button class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</button>-->

</div>

<!-- Content Row -->
<div class="row">
    
    <!-- Content Column -->
    <div class="col-lg-12 mb-4">
        <!-- Project Card Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Editar</h6>
            </div>
            <div class="card-body">
                
                <div class="row">
                    <div class="col-lg-2 mb-4">
                        <label for="exampleFormControlSelect1">Tipo:</label>
                        <select class="form-control" id="tipo-user">
                            <option value="1" <?=($getUserInfo->user_tipo == 1) ? 'selected' : ''?>>Aluno</option>
                            <option value="2" <?=($getUserInfo->user_tipo == 2) ? 'selected' : ''?>>Não aluno</option>
                        </select>
                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="mb-3">
                            <label for="exampleFormControlInput1">Nome:</label>
                            <input class="form-control" id="nome-user" type="text" placeholder="Insira um nome..." value="<?=$getUserInfo->user_nome?>">
                        </div>
                    </div>
                    <div class="col-lg-2 mb-4">
                        <div class="mb-3">
                            <label for="exampleFormControlInput1">Login:</label>
                            <input class="form-control" id="login-user" type="text" placeholder="Insira um login..." value="<?=$getUserInfo->user_login?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 mb-4">
                        <div class="mb-3">
                            <label for="exampleFormControlInput1">Email:</label>
                            <input class="form-control" id="email-user" type="text" placeholder="Insira uma email..." value="<?=$getUserInfo->user_email?>">
                        </div>
                    </div>
                    <div class="col-lg-2 mb-4">
                        <label for="exampleFormControlSelect1">Turma:</label>
                        <select class="form-control" id="turma-user">
                            <option val="">Selecione...</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 mb-2">
                    </div>
                    <div class="col-lg-4 mb-2 text-center">
                        <button id="send_update_user" class="btn btn-success"><i class="far fa-check-circle"></i> Enviar</button>
                        <a href="#/adm_ger_usuario/" class="btn btn-secondary"><i class="fas fa-arrow-left"></i> Voltar</a>
                    </div>
                    <div class="col-lg-4 mb-2">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script>

$("#send_update_user").on('click',function(){

    let user_id = "<?=$_GET['user_id']?>";

    let tipo    = $('#tipo-user').val();
    let nome    = $('#nome-user').val();
    let login   = $('#login-user').val();
    let email   = $('#email-user').val();
    //let turma = $('#turma-user').val();

    if(nome == '' || login == '' || email == ''){
        toastr.error('Por favor, informe todos os campos obrigatórios!');
		return false;
    }
    
    $.ajax({
        type: "POST",
        url: "src/api/adm/adm_gerenciamento.php",
        data: { 
            type: 'updateUser', 
            user_id: user_id,
            tipo: tipo,
            nome: nome,
            login: login,
            email: email,
            //turma: turma,
        },
        beforeSend: function(){
            $('#cover-spin').show();
        },
        success: function (res) {
            alert(res)
            res = JSON.parse(res);

            if(res.status == 1){
                Swal.fire({
                    title: 'Tudo certo!',
                    html: 'O usuário foi atualizado com sucesso.',
                    type: "success",
                    onClose:function(){
                        location.reload();
                    }
                });
            }
            else{
                Swal.fire({
                    title: 'Ops!',
                    html: "ocorreu um erro. Tente novamente",
                    type: "error"
                });
            }

            $('#cover-spin').hide();
        },
        error:function(xhr, status, error){
            let errorMessage = xhr.status + ': ' + xhr.statusText
            alert(errorMessage);
        }
    });

});    

</script>