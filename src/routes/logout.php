<?php

sleep(1);
session_start();

$pageRedirect = "Location: ../../";

if($_SESSION['user_tipo'] == 0){
    $pageRedirect = "Location: ../../adm";
}

unset($_SESSION['user_id']);
unset($_SESSION['user_matricula']);
unset($_SESSION['user_nome']);
unset($_SESSION['user_tipo']);
unset($_SESSION['user_turma']);
session_destroy();
header($pageRedirect);

?>