
let app = $.sammy(function () {

    this.get('#/duvida_zero/', function () {
        $(".nav-item").removeClass("active");
        $("#nav-item-dv").addClass("active");
        $(".container-fluid").empty();
        $(".container-fluid").append('<div id="cover-spin"><img src="assets/img/6.svg" style="height:110px;display:block;margin:auto;margin-top:250px;"><h3 class="text-center">Aguarde...</h3></div>');
        setTimeout(function(){ $(".container-fluid").load('src/pages/avaliacoes/duvida_zero.php'); }, 1000);
    });

    this.get('#/duvida_zero_iniciar/', function () {
        $(".nav-item").removeClass("active");
        $("#nav-item-dv").addClass("active");
        $(".container-fluid").empty();
        $(".container-fluid").append('<div id="cover-spin"><img src="assets/img/6.svg" style="height:110px;display:block;margin:auto;margin-top:250px;"><h3 class="text-center">Aguarde...</h3></div>');
        setTimeout(function(){ $(".container-fluid").load('src/pages/avaliacoes/duvida_zero_iniciar.php?id_dz='+((window.location.href).split('?')[1]).split('=')[1]); }, 1000);
    });

    this.get('#/duvida_zero_exame/', function () {
        $(".nav-item").removeClass("active");
        $("#nav-item-dv").addClass("active");
        $(".container-fluid").empty();
        $(".container-fluid").append('<div id="cover-spin"><img src="assets/img/6.svg" style="height:110px;display:block;margin:auto;margin-top:250px;"><h3 class="text-center">Aguarde...</h3></div>');
        setTimeout(function(){ $(".container-fluid").load('src/pages/avaliacoes/duvida_zero_exame.php?id_dz='+((window.location.href).split('?')[1]).split('=')[1]); }, 1000);
    });


});

app.run();