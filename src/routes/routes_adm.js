
let app = $.sammy(function () {

    //************************* DÚVIDA ZERO *************************//

    this.get('#/adm_criar_duvida_zero/', function () {
        $(".collapse-item").removeClass("active");
        $("#nav-item-criar-dz").addClass("active");
        $(".container-fluid").empty();
        $(".container-fluid").append('<div id="cover-spin"><img src="assets/img/6_blue.svg" style="height:110px;display:block;margin:auto;margin-top:250px;"><h3 class="text-center">Aguarde...</h3></div>');
        setTimeout(function(){ $(".container-fluid").load('src/pages/adm/avaliacoes/duvida_zero_criar.php'); }, 1000);
    });

    this.get('#/adm_ger_duvida_zero/', function () {
        $(".collapse-item").removeClass("active");
        $("#nav-item-ger-dz").addClass("active");
        $(".container-fluid").empty();
        $(".container-fluid").append('<div id="cover-spin"><img src="assets/img/6_blue.svg" style="height:110px;display:block;margin:auto;margin-top:250px;"><h3 class="text-center">Aguarde...</h3></div>');
        setTimeout(function(){ $(".container-fluid").load('src/pages/adm/avaliacoes/duvida_zero_ger.php'); }, 1000);
    });

    this.get('#/adm_editar_duvida_zero/', function () {
        $(".collapse-item").removeClass("active");
        $("#nav-item-ger-dz").addClass("active");
        $(".container-fluid").empty();
        $(".container-fluid").append('<div id="cover-spin"><img src="assets/img/6_blue.svg" style="height:110px;display:block;margin:auto;margin-top:250px;"><h3 class="text-center">Aguarde...</h3></div>');
        setTimeout(function(){ $(".container-fluid").load('src/pages/adm/avaliacoes/duvida_zero_editar.php?id_dz='+((window.location.href).split('?')[1]).split('=')[1]); }, 1000);
    });

    this.get('#/adm_duvida_zero_ranking/', function () {
        $(".collapse-item").removeClass("active");
        $("#nav-item-ger-dz").addClass("active");
        $(".container-fluid").empty();
        $(".container-fluid").append('<div id="cover-spin"><img src="assets/img/6_blue.svg" style="height:110px;display:block;margin:auto;margin-top:250px;"><h3 class="text-center">Aguarde...</h3></div>');
        setTimeout(function(){ $(".container-fluid").load('src/pages/adm/avaliacoes/duvida_zero_ranking.php?id_dz='+((window.location.href).split('?')[1]).split('=')[1]); }, 1000);
    });


    //************************* GERENCIAMENTO *************************//

    this.get('#/adm_criar_usuario/', function () {
        $(".collapse-item").removeClass("active");
        $("#nav-item-criar-usuario").addClass("active");
        $(".container-fluid").empty();
        $(".container-fluid").append('<div id="cover-spin"><img src="assets/img/6_blue.svg" style="height:110px;display:block;margin:auto;margin-top:250px;"><h3 class="text-center">Aguarde...</h3></div>');
        setTimeout(function(){ $(".container-fluid").load('src/pages/adm/gerenciamento/usuario_criar.php'); }, 1000);
    });

    this.get('#/adm_ger_usuario/', function () {
        $(".collapse-item").removeClass("active");
        $("#nav-item-ger-usuario").addClass("active");
        $(".container-fluid").empty();
        $(".container-fluid").append('<div id="cover-spin"><img src="assets/img/6_blue.svg" style="height:110px;display:block;margin:auto;margin-top:250px;"><h3 class="text-center">Aguarde...</h3></div>');
        setTimeout(function(){ $(".container-fluid").load('src/pages/adm/gerenciamento/usuario_ger.php'); }, 1000);
    });

    this.get('#/adm_editar_usuario/', function () {
        $(".collapse-item").removeClass("active");
        $("#nav-item-ger-usuario").addClass("active");
        $(".container-fluid").empty();
        $(".container-fluid").append('<div id="cover-spin"><img src="assets/img/6_blue.svg" style="height:110px;display:block;margin:auto;margin-top:250px;"><h3 class="text-center">Aguarde...</h3></div>');
        setTimeout(function(){ $(".container-fluid").load('src/pages/adm/gerenciamento/usuario_editar.php?user_id='+((window.location.href).split('?')[1]).split('=')[1]); }, 1000);
    });


});

app.run();