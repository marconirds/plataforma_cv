<?php

//ini_set('display_errors',1);
//ini_set('display_startup_erros',1);
//error_reporting(E_ALL);

require_once('../../../vendor/autoload.php');

use Plataforma_CV\Classes\Gerenciamento;

switch($_POST['type']){

    case "enableDisableStatusUser":

        $return     = Array();
        $objGer     = new Gerenciamento();

        $id_user    =  $_POST['id_user'];
        $status     =  $_POST['status'];

        $array = Array(
            'user_status'        => $status
        );

        $updateStatusUser = $objGer->updateStatusUser($id_user,$array);
        
        if($updateStatusUser){

            $return['status']   = 1;

        }
        else{
            $return['status']   = 0;
        }

        echo json_encode($return);
    
    break;

    case "createUser":

        $return     = Array();
        $objGer     = new Gerenciamento();

        $tipo       =  $_POST['tipo'];
        $nome       =  $_POST['nome'];
        $login      =  $_POST['login'];
        $email      =  $_POST['email'];
        $senha      =  $_POST['senha'];
        //$turma    =  $_POST['titulo'];
        $status     =  $_POST['status'];

        $array = Array(
            'user_nome'         => $nome,
            'user_login'        => $login,
            'user_pass'         => crypt($senha),
            'user_nicename'     => $login,
            'user_email'        => $email,
            'user_status'       => $status,
            'display_name'      => mb_strtoupper(explode(' ',$nome)[0],'UTF-8'),
            'user_tipo'         => $tipo
        );

        $createNewUser = $objGer->createNewUser($array);
        
        if($createNewUser){

            $return['status']   = 1;

        }
        else{
            $return['status']   = 0;
        }

        echo json_encode($return);
    
    break;

    case "updateUser":

        $return     = Array();
        $objGer     = new Gerenciamento();

        $user_id    =  $_POST['user_id'];
        $tipo       =  $_POST['tipo'];
        $nome       =  $_POST['nome'];
        $login      =  $_POST['login'];
        $email      =  $_POST['email'];
        //$turma    =  $_POST['titulo'];

        $array = Array(
            'user_nome'         => $nome,
            'user_login'        => $login,
            'user_nicename'     => $login,
            'user_email'        => $email,
            'display_name'      => mb_strtoupper(explode(' ',$nome)[0],'UTF-8'),
            'user_tipo'         => $tipo
        );

        $updateUser = $objGer->updateUser($user_id,$array);
        
        if($updateUser){

            $return['status']   = 1;

        }
        else{
            $return['status']   = 0;
        }

        echo json_encode($return);
    
    break;

    case "deleteUser":

        $return     = Array();
        $objGer     = new Gerenciamento();

        $user_id    =  $_POST['user_id'];

        $deleteUser = $objGer->deleteUser($user_id);
        
        if($deleteUser){

            $return['status']   = 1;

        }
        else{
            $return['status']   = 0;
        }

        echo json_encode($return);
    
    break;

}