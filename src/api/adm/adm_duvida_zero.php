<?php

ini_set('display_errors',1);
ini_set('display_startup_erros',1);
error_reporting(E_ALL);

require_once('../../../vendor/autoload.php');
require_once('../../../vendor/json-format/src/format_json.php');

use Plataforma_CV\Classes\Duvida_zero;

switch($_POST['type']){

    case "getJsonFromFile":

        $id_dz          =  $_POST['id_dz'];
        $jsonQuestoes   = file_get_contents('../../json/dz_'.$id_dz.'.json');

        echo $jsonQuestoes;
        
    break;

    case "viewRespostasDZAluno":

        $objDZ                  = new Duvida_zero();
        $id_dz_aluno            =  $_POST['id_dz_aluno'];

        
        $getRespotas            = $objDZ->getRespotas($id_dz_aluno);
        $getRespotasJson        = json_decode($getRespotas->questoes_respondidas);

        $questoesCorretas       = file_get_contents('../../json/dz_'.$getRespotas->id_dz.'.json');
        $questoesCorretas       = json_decode($questoesCorretas);

        $newArray = Array();

        foreach($getRespotasJson as $x => $r){
            $newArray[$x]['resposta'] = $getRespotasJson->$x;
            $newArray[$x]['gabarito'] = $questoesCorretas->$x->gabarito;
            //$getRespotasJson[$x.'_gabarito'] = $questoesCorretas->$x->gabarito;
        }

        echo json_encode($newArray);
        
    break;

    case "createDuvidaZero":

        $return = Array();
        $objDZ  = new Duvida_zero();

        $titulo     =  $_POST['titulo'];
        $status     =  $_POST['status'];
        $questoes   =  $_POST['questoes'];

        $array = Array(
            'titulo'        => $titulo,
            'status'        => $status
        );

        $createNewDZ = $objDZ->createNewDZ($array);
        
        if($createNewDZ){

            $return['status']   = 1;

            $formattedJsonQuestoes = format_json($questoes);

            $objectJson = json_decode($formattedJsonQuestoes,true,512,JSON_UNESCAPED_UNICODE);

            foreach($objectJson as $i => $p){
                if($objectJson[$i]['descricao'] == ''){
                    unset($objectJson[$i]);
                }
            }

            $objectJson             = json_encode($objectJson,JSON_UNESCAPED_UNICODE);
            $formattedJsonQuestoes  = format_json($objectJson);

            $fp = fopen('../../json/dz_'.$createNewDZ.'.json', 'w');
            fwrite($fp,$formattedJsonQuestoes);
            fclose($fp);
        }
        else{
            $return['status']   = 0;
        }

        echo json_encode($return);
    
    break;

    case "updateDuvidaZero":

        $return = Array();
        $objDZ  = new Duvida_zero();

        $id_dz      =  $_POST['id_dz'];
        $titulo     =  $_POST['titulo'];
        $questoes   =  $_POST['questoes'];

        $array = Array(
            'titulo'        => $titulo
        );

        $updateDZ = $objDZ->updateDZ($id_dz,$array);
        
        if($updateDZ){

            $return['status']   = 1;

            $formattedJsonQuestoes = format_json($questoes);

            $objectJson = json_decode($formattedJsonQuestoes,true,512,JSON_UNESCAPED_UNICODE);

            foreach($objectJson as $i => $p){
                if($objectJson[$i]['descricao'] == ''){
                    unset($objectJson[$i]);
                }
            }

            $objectJson             = json_encode($objectJson,JSON_UNESCAPED_UNICODE);
            $formattedJsonQuestoes  = format_json($objectJson);

            $fp = fopen('../../json/dz_'.$id_dz.'.json', 'w');
            fwrite($fp,$formattedJsonQuestoes);
            fclose($fp);
        }
        else{
            $return['status']   = 0;
        }

        echo json_encode($return);
    
    break;

    case "deleteDZ":

        $return     = Array();
        $objDZ      = new Duvida_zero();

        $id_dz    =  $_POST['id_dz'];

        $deleteDZ = $objDZ->deleteDZ($id_dz);
        
        if($deleteDZ){

            $return['status']   = 1;

        }
        else{
            $return['status']   = 0;
        }

        echo json_encode($return);
    
    break;

    case "deleteDZAluno":

        $return         = Array();
        $objDZ          = new Duvida_zero();

        $id_dz_aluno    =  $_POST['id_dz_aluno'];

        $deleteDZAluno = $objDZ->deleteDZAluno($id_dz_aluno);
        
        if($deleteDZAluno){

            $return['status']   = 1;

        }
        else{
            $return['status']   = 0;
        }

        echo json_encode($return);
    
    break;

    case "enableDisableStatusDZ":

        $return     = Array();
        $objDZ      = new Duvida_zero();

        $id_dz      =  $_POST['id_dz'];
        $status     =  $_POST['status'];

        $array = Array(
            'status'        => $status
        );

        $updateStatusDZ = $objDZ->updateStatusDZ($id_dz,$array);
        
        if($updateStatusDZ){

            $return['status']   = 1;

        }
        else{
            $return['status']   = 0;
        }

        echo json_encode($return);
    
    break;

    case "enableDisableRankingDZ":

        $return     = Array();
        $objDZ      = new Duvida_zero();

        $id_dz      =  $_POST['id_dz'];
        $status     =  $_POST['status'];

        $array = Array(
            'mostrar_ranking'        => $status
        );

        $updateStatusDZ = $objDZ->updateStatusDZ($id_dz,$array);
        
        if($updateStatusDZ){

            $return['status']   = 1;

        }
        else{
            $return['status']   = 0;
        }

        echo json_encode($return);
    
    break;

}