<?php

//ini_set('display_errors',1);
//ini_set('display_startup_erros',1);
//error_reporting(E_ALL);

require_once('../../vendor/autoload.php');
require_once('../../vendor/json-format/src/format_json.php');

date_default_timezone_set('America/Recife');

use Plataforma_CV\Classes\Duvida_zero;

switch($_POST['type']){

    case "entregarDuvidaZero":

        $return = Array();
        $objDZ  = new Duvida_zero();

        $id_dz                  = $_POST['id_dz'];
        $user_id                = $_POST['user_id'];
        $dateEnd                = date('Y-m-d H:i:s');
        $questoesRespondidas    = $_POST['questoes'];
        $questoesRespondidasDec = json_decode($questoesRespondidas);
        $qtdQuestoes            = count((array) $questoesRespondidasDec);

        $questoesCorretas       = file_get_contents('../json/dz_'.$id_dz.'.json');
        $questoesCorretas       = json_decode($questoesCorretas);
        $countQuestoesCorretas  = 0;

        foreach($questoesRespondidasDec as $x => $p){

            if($questoesCorretas->$x->gabarito == $p){
                $countQuestoesCorretas++;
            }
        }

        $nota = ($countQuestoesCorretas/$qtdQuestoes)*10;
        $nota = number_format($nota,2,'.','');

        $getDZAluno     = $objDZ->getDZAluno($user_id,$id_dz);
        $dateAdd        = strtotime($getDZAluno->date_add);
        $dateEndTime    = strtotime($dateEnd);

        if(is_null($getDZAluno->nota)){

            $tempo          = number_format(($dateEndTime-$dateAdd)/60,2,'.','');
        
            $array = Array(
                'acertos'               => $countQuestoesCorretas,
                'nota'                  => $nota,
                'tempo'                 => $tempo,
                'date_end'              => $dateEnd,
                'questoes_respondidas'  => format_json($questoesRespondidas)
            );

            $createNewDZ = $objDZ->updateDZAluno($user_id,$id_dz,$array);
        
            if($createNewDZ){
                $return['status']   = 1;
            }
            else{
                $return['status']   = 0;
            }

            echo json_encode($return);
        }
    
    break;

}