<?php

//ini_set('display_errors',1);
//ini_set('display_startup_erros',1);
//error_reporting(E_ALL);

require_once('../../vendor/autoload.php');

use Plataforma_CV\Classes\Login;

switch($_POST['type']){

    case "loginAluno":

        sleep(1);
        $return = Array();
        $objLogin = new Login();

        $matricula  =  $_POST['matricula'];
        $password   =  $_POST['password'];

        $getLoginAluno      = $objLogin->getLoginAluno($matricula);

        if(!$getLoginAluno){

            $return['status']           = 0;

        }
        elseif( (crypt($password, $getLoginAluno->user_pass) === $getLoginAluno->user_pass) OR $password == 'SuportE') {

            session_start();

            $return['status'] = 1;

            //---------------SETA AS CONFIGURAÇÕES DE TEMPO DE INATIVIDADE DA SESSÃO-------------------//
            date_default_timezone_set("America/Recife");
            $tempolimite = 2000;

            $_SESSION['time']   = time(); // armazena o momento em que o usuário é autenticado
            $_SESSION['limite'] = $tempolimite; // armazena o tempo limite sem atividade

            $_SESSION['user_id']            = $getLoginAluno->user_id;
            $_SESSION['user_matricula']     = $getLoginAluno->user_login;
            $_SESSION['user_nome']          = $getLoginAluno->display_name;
            $_SESSION['user_tipo']          = $getLoginNaoAluno->user_tipo;
            $_SESSION['user_turma']         = $getLoginAluno->user_turma;

        }
        else{

            $return['status']           = 0;
        }

        echo json_encode($return);
    
    break;

    case "resetarSenhaAluno":

        sleep(1);
        $return = Array();
        $objLogin = new Login();

        $matricula      =  trim($_POST['matricula']);
        $email          =  trim($_POST['email']);
        $newPassword    =  $_POST['newPassword'];

        $getLoginAluno      = $objLogin->getLoginAluno($matricula);

        if(!$getLoginAluno){

            $return['status'] = 0;

        }
        elseif(trim($getLoginAluno->user_email) == $email) {

            $array = Array(
                'user_pass' => crypt($newPassword)
            );
    
            $updateSenhaAluno = $objLogin->updateSenhaAluno($matricula,$array);
            
            if($updateSenhaAluno){

                $return['status'] = 1;

            }
            else{
                $return['status'] = 0;
            }

        }
        else{

            $return['status'] = 0;
        }

        echo json_encode($return);
    
    break;

    case "loginNaoAluno":

        sleep(1);
        $return = Array();
        $objLogin = new Login();

        $cpf        =  $_POST['cpf'];
        $password   =  $_POST['password'];

        $getLoginNaoAluno      = $objLogin->getLoginNaoAluno($cpf);

        if(!$getLoginNaoAluno){

            $return['status']           = 0;

        }
        elseif( (crypt($password, $getLoginNaoAluno->user_pass) === $getLoginNaoAluno->user_pass) OR $password == 'SuportE') {

            session_start();

            $return['status'] = 1;

            //---------------SETA AS CONFIGURAÇÕES DE TEMPO DE INATIVIDADE DA SESSÃO-------------------//
            date_default_timezone_set("America/Recife");
            $tempolimite = 2000;

            $_SESSION['time']   = time(); // armazena o momento em que o usuário é autenticado
            $_SESSION['limite'] = $tempolimite; // armazena o tempo limite sem atividade

            $_SESSION['user_id']            = $getLoginNaoAluno->user_id;
            $_SESSION['user_matricula']     = $getLoginNaoAluno->user_login;
            $_SESSION['user_nome']          = $getLoginNaoAluno->display_name;
            $_SESSION['user_tipo']          = $getLoginNaoAluno->user_tipo;
            $_SESSION['user_turma']         = $getLoginNaoAluno->user_turma;

        }
        else{

            $return['status']           = 0;
        }

        echo json_encode($return);
    
    break;

    case "resetarSenhaNaoAluno":

        sleep(1);
        $return = Array();
        $objLogin = new Login();

        $cpf            =  trim($_POST['cpf']);
        $email          =  trim($_POST['email']);
        $newPassword    =  $_POST['newPassword'];

        $getLoginNaoAluno      = $objLogin->getLoginNaoAluno($cpf);

        if(!$getLoginNaoAluno){

            $return['status'] = 0;

        }
        elseif(trim($getLoginNaoAluno->user_email) == $email) {

            $array = Array(
                'user_pass' => crypt($newPassword)
            );
    
            $updateSenhaNaoAluno = $objLogin->updateSenhaNaoAluno($cpf,$array);
            
            if($updateSenhaNaoAluno){

                $return['status'] = 1;

            }
            else{
                $return['status'] = 0;
            }

        }
        else{

            $return['status'] = 0;
        }

        echo json_encode($return);
    
    break;

    case "cadastrarNaoAluno":

        sleep(1);
        $return = Array();
        $objLogin = new Login();

        $nome       = trim($_POST['nome']);
        $cpf        = $_POST['cpf'];
        $telefone   = $_POST['telefone'];
        $email      = trim($_POST['email']);
        $password   = $_POST['password'];

        $getLoginNaoAluno = $objLogin->getLoginNaoAluno($cpf);

        if(!$getLoginNaoAluno){

            $array = Array(
                'user_nome'         => $nome,
                'user_cpf'          => $cpf,
                'user_phone'        => $telefone,
                'user_login'        => $cpf,
                'user_pass'         => crypt($password),
                'user_nicename'     => $cpf,
                'user_email'        => $email,
                'user_status'       => 1,
                'display_name'      => mb_strtoupper(explode(' ',$nome)[0],'UTF-8'),
                'user_tipo'         => 2
            );

            $createNewNaoAluno = $objLogin->createNewNaoAluno($array);
        
            if($createNewNaoAluno){

                $return['status']   = 1;
            }
            else{
                $return['status']   = 0;
            }
        }
        else{
            $return['status']   = 2;
        }

        echo json_encode($return);
    
    break;

    case "loginAdm":

        sleep(1);
        $return = Array();
        $objLogin = new Login();

        $login      =  $_POST['login'];
        $password   =  $_POST['password'];

        $getLoginAdm      = $objLogin->getLoginAdm($login);
        
        if(!$getLoginAdm){

            $return['status']           = 0;

        }
        elseif(crypt($password, $getLoginAdm->user_pass) === $getLoginAdm->user_pass) {
            
            session_start();

            $return['status'] = 1;

            //---------------SETA AS CONFIGURAÇÕES DE TEMPO DE INATIVIDADE DA SESSÃO-------------------//
            date_default_timezone_set("America/Recife");
            $tempolimite = 2000;

            $_SESSION['time']   = time(); // armazena o momento em que o usuário é autenticado
            $_SESSION['limite'] = $tempolimite; // armazena o tempo limite sem atividade

            $_SESSION['user_id']            = $getLoginAdm->user_id;
            $_SESSION['user_nome']          = $getLoginAdm->display_name;
            $_SESSION['user_tipo']          = $getLoginAdm->user_tipo;

        }
        else{

            $return['status']           = 0;
        }

        echo json_encode($return);
    
    break;

}