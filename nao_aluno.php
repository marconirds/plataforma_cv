<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Plataforma | Carlos Valença</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/sb-admin-2/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="vendor/sb-admin-2/css/sb-admin-2.min.css" rel="stylesheet">

    <link href="assets/css/spinner-full.css" rel="stylesheet">
    <link href="vendor/toastr/toastr.min.css" rel="stylesheet" />
	<link href="vendor/sweet-alert-2/sweetalert2.min.css" rel="stylesheet" />
    <link href="assets/css/scroll-bar.css" rel="stylesheet" />

    <style>
    
        #toast-container > div {
            opacity: 1 !important; 
        }
    
    </style>

</head>

<body class="bg-gradient-primary">

    <div id="cover-spin"><img src='assets/img/6.svg' style="height:110px;display:block;margin:auto;margin-top:250px;"><h3 class="text-center">Aguarde...</h3></div>

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center mb-5">
                                        <img style="width:250px" src="assets/img/cropped-carlos-valenca-logo.png"><br><br>
                                        <h1 class="h4 text-gray-900">Área do Não aluno</h1>
                                    </div>
                                    <div class="user">
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-user"
                                                id="cpf" aria-describedby="emailHelp"
                                                placeholder="Informe o CPF" onkeypress="return isNumber(event)">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user"
                                                id="password" placeholder="Senha">
                                        </div>
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" class="custom-control-input" id="customCheck">
                                                <label class="custom-control-label" for="customCheck">Manter
                                                    conectado</label>
                                            </div>
                                        </div>
                                        <button id="login" class="btn btn-success btn-user btn-block">
                                            <i class="fas fa-unlock-alt"></i> Acessar
                                        </button>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-lg-6 text-left">
                                            <a class="small" href="#" data-toggle="modal" data-target="#resetPasswordModal">Esqueceu a senha?</a>                                                                           
                                        </div>
                                        <div class="col-lg-6 text-right">
                                            <a class="small" href="#" data-toggle="modal" data-target="#newAccountModal">Criar nova conta</a>                                                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- <footer class="footer">
        <div class="container-fluid">
          
          <div class="copyright">
            ©
            <script>
              document.write(new Date().getFullYear())
            </script> Carlos valença Biologia 
          </div>
        </div>
    </footer> -->


    <!-- Reset Password Modal-->
    <div class="modal fade" id="resetPasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Esqueceu a senha?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="mb-3">
                                <label for="exampleFormControlInput1">CPF:</label>
                                <input class="form-control" id="resetCPF" type="text" placeholder="" onkeypress="return isNumber(event)">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="mb-3">
                                <label for="exampleFormControlInput1">Email:</label>
                                <input class="form-control" id="resetEmail" type="text" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="mb-3">
                                <label for="exampleFormControlInput1">Nova senha:</label>
                                <input class="form-control" id="resetPassword" type="password" placeholder="">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button id="sendResetPassword" class="btn btn-success"><i class="fas fa-redo"></i> Resetar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- New Account Modal-->
    <div class="modal fade" id="newAccountModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cadastro Não Aluno</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="mb-3">
                                <label for="exampleFormControlInput1">Nome completo:</label>
                                <input class="form-control" id="cadastrarNome" type="text" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="mb-3">
                                <label for="exampleFormControlInput1">CPF:</label>
                                <input class="form-control" id="cadastrarCPF" type="text" placeholder="" onkeypress="return isNumber(event)"  maxlength="11">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="mb-3">
                                <label for="exampleFormControlInput1">Telefone:</label>
                                <input class="form-control" id="cadastrarTelefone" type="text" placeholder="" onkeypress="return isNumber(event)"  maxlength="15">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="mb-3">
                                <label for="exampleFormControlInput1">Email:</label>
                                <input class="form-control" id="cadastrarEmail" type="text" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="mb-3">
                                <label for="exampleFormControlInput1">Senha:</label>
                                <input class="form-control" id="cadastrarPassword" type="password" placeholder="">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <button id="sendCadastrar" class="btn btn-success"><i class="far fa-check-circle"></i> Cadastrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/sb-admin-2/vendor/jquery/jquery.min.js"></script>
    <script src="vendor/sb-admin-2/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/sb-admin-2/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="vendor/sb-admin-2/js/sb-admin-2.min.js"></script>

    <script src="vendor/toastr/toastr.min.js"></script>
	<script src="vendor/sweet-alert-2/sweetalert2.min.js"></script>

</body>


<script>

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

$("#login").on('click', function(){

    let cpf         = $("#cpf").val();
    let password    = $("#password").val();

    if(cpf == '' || password == ''){
        toastr.error('Por favor, informe o CPF e a senha!');
		return false;
    }

    login(cpf,password);

});

function login(cpf,password){
	
	$.ajax({
		type: "POST",
		url: "src/api/login.php",
		data: { 
			type: 'loginNaoAluno', 
            cpf:cpf,
			password: password
		},
		beforeSend: function () {
			$('#cover-spin').show();
        },
		success: function (response) {

			$('#cover-spin').hide();

			response = JSON.parse(response);

			if(response.status == 1){
				window.location.href = 'main#/duvida_zero/';
			}
			else{
                toastr.error('CPF ou Senha incorretos! Tente novamente.');
		        return false;
			}
		},
		error: function (response) { 

			$('#cover-spin').hide();
			alert(response);
		}
	});

}

$("#sendResetPassword").on('click', function(){

    let cpf         = $("#resetCPF").val();
    let email       = $("#resetEmail").val();
    let newPassword = $("#resetPassword").val();

    if(cpf == '' || email == '' || newPassword == ''){
        toastr.error('Por favor, informe todos os campos!');
		return false;
    }

    resetarSenha(cpf,email,newPassword);

});

function resetarSenha(cpf,email,newPassword){
	
	$.ajax({
		type: "POST",
		url: "src/api/login.php",
		data: { 
			type: 'resetarSenhaNaoAluno', 
            cpf: cpf,
			email: email,
            newPassword: newPassword
		},
		beforeSend: function () {
            $('#resetPasswordModal').modal('hide');
			$('#cover-spin').show();
        },
		success: function (response) {

			$('#cover-spin').hide();

			response = JSON.parse(response);

			if(response.status == 1){
				Swal.fire({
                    title: 'Tudo certo!',
                    html: 'Sua senha foi redefinida.',
                    type: "success",
                    onClose:function(){
                        location.reload();
                    }
                });
			}
			else{
		        Swal.fire({
                    title: 'Ops!',
                    html: 'CPF ou Email incorretos. Tente novamente.',
                    type: "error",
                    onClose:function(){
                        $('#resetPasswordModal').modal('show');
                    }
                });
			}
		},
		error: function (response) { 

			$('#cover-spin').hide();
			alert(response);
		}
	});

}


$("#sendCadastrar").on('click', function(){

    let nome        = $("#cadastrarNome").val();
    let cpf         = $("#cadastrarCPF").val();
    let telefone    = $("#cadastrarTelefone").val();
    let email       = $("#cadastrarEmail").val();
    let password       = $("#cadastrarPassword").val();

    if(nome == '' || cpf == '' || telefone == '' || email == '' || password == ''){
        toastr.error('Por favor, informe todos os campos!');
		return false;
    }

    cadastrarnaoAluno(nome,cpf,telefone,email,password);

});

function cadastrarnaoAluno(nome,cpf,telefone,email,password){
	
	$.ajax({
		type: "POST",
		url: "src/api/login.php",
		data: { 
			type: 'cadastrarNaoAluno', 
            nome: nome,
            cpf: cpf,
            telefone: telefone,
            email: email,
            password: password
		},
		beforeSend: function () {
            $('#newAccountModal').modal('hide');
			$('#cover-spin').show();
        },
		success: function (response) {

			$('#cover-spin').hide();

			response = JSON.parse(response);

			if(response.status == 1){
				Swal.fire({
                    title: 'Tudo certo!',
                    html: 'Sua conta foi cadastrada.',
                    type: "success",
                    onClose:function(){
                        location.reload();
                    }
                });
			}
            else if(response.status == 2){
				Swal.fire({
                    title: 'Ops!',
                    html: 'Já existe um cadastro com esse CPF. Por favor, resete sua senha.',
                    type: "error",
                    onClose:function(){
                        location.reload();
                    }
                });
			}
			else{
		        Swal.fire({
                    title: 'Ops!',
                    html: 'Ocorreu um erro ao realizar o cadastro. Tente novamente.',
                    type: "error",
                    onClose:function(){
                        $('#newAccountModal').modal('show');
                    }
                });
			}
		},
		error: function (response) { 

			$('#cover-spin').hide();
			alert(response);
		}
	});

}


toastr.options = {
  "closeButton": true,
  "debug": false,
  "progressBar": false,
  "preventDuplicates": true,
  "positionClass": "toast-top-right",
  "onclick": null,
  "showDuration": "100",
  "hideDuration": "100",
  "timeOut": "4000",
  "extendedTimeOut": "1000",
  "showEasing": "easeOutBounce",
  "hideEasing": "linear",
  "showMethod": "slideDown",
  "hideMethod": "slideUp"
}


</script>

</html>